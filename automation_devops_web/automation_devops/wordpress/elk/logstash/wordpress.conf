input {
  beats {
    port => 5044
  }
}
filter {
	# Extract Authentication Logs
	grok { 
		match => { "message" => "(?<event_time>%{YEAR}-%{MONTHNUM}-%{MONTHDAY}\s%{TIME})\s\w+\s%{HOSTNAME:host_name}\s.*\s:\s(?<log_level>\w+):\s%{IPORHOST:src_ip};\s(?<msg>.*):\s(?<user_name>\w+)" }
		add_tag => "authentication"
	}
	# plugins activation and deactivation logs;
	grok { 
		match => { "message" => "(?<event_time>%{YEAR}-%{MONTHNUM}-%{MONTHDAY}\s%{TIME})\s\w+\s%{HOSTNAME:host_name}\s.*\s:\s(?<log_level>\w+):\s(?<user_name>\w+),\s%{IPORHOST:src_ip};\s(?<msg>Plugin.*):\s(?<plugin>.*)" }
		add_tag => "plugins"
	}
	# Blogs Posts Management
	## New draft post/page, publish post/pages, delete/restore posts/pages, 
	grok {
		match => { "message" => "(?<event_time>%{YEAR}-%{MONTHNUM}-%{MONTHDAY}\s%{TIME})\s\w+\s%{HOSTNAME:host_name}\s.*\s:\s(?<log_level>\w+):\s(?<user_name>\w+),\s%{IPORHOST:src_ip};\s(?<msg>Post.*|Page.*);\s.*Old\sstatus:\s(?<old_status>\w.+),New\sstatus:\s(?<new_status>\w.+),Title:\s(?<title>.*)" }
		add_tag => "posts_pages"
	}
	## Updating Existing draft post/page;
	grok {
		match => { "message" => "(?<event_time>%{YEAR}-%{MONTHNUM}-%{MONTHDAY}\s%{TIME})\s\w+\s%{HOSTNAME:host_name}\s.*\s:\s(?<log_level>\w+):\s(?<user_name>\w+),\s%{IPORHOST:src_ip};\s(?<msg>Revision.*);\s.*Old\sstatus:\s(?<old_status>\w.+),New\sstatus:\s(?<new_status>\w.+),Title:\s(?<title>.*)" }
		add_tag => "posts_pages"
	}
	## User Account Created
	grok {
		match => { "message" => "(?<event_time>%{YEAR}-%{MONTHNUM}-%{MONTHDAY}\s%{TIME})\s\w+\s%{HOSTNAME:host_name}\s.*\s:\s(?<log_level>\w+):\s(?<created_by>\w+),\s%{IPORHOST:src_ip};\s(?<msg>User account created);.*name:\s(?<user_name>\w+);\semail:\s(?<email_address>[a-zA-Z0-9_.+=:-]+@[0-9A-Za-z][0-9A-Za-z-]{0,62}(?:\.(?:[0-9A-Za-z][0-9A-Za-z-]{0,62})));\sroles:\s(?<user_role>\w.+)" }
		add_tag => "account_created"
	}
	## User Account Changes
	grok {
		match => { "message" => "(?<event_time>%{YEAR}-%{MONTHNUM}-%{MONTHDAY}\s%{TIME})\s\w+\s%{HOSTNAME:host_name}\s.*\s:\s(?<log_level>\w+):\s(?<edited_by>\w+),\s%{IPORHOST:src_ip};\s(?<msg>User account edited);.*name:\s(?<user_name>\w+);\sold_name:\s(?<old_name>\w+);\semail:\s(?<email_address>[a-zA-Z0-9_.+=:-]+@[0-9A-Za-z][0-9A-Za-z-]{0,62}(?:\.(?:[0-9A-Za-z][0-9A-Za-z-]{0,62})));\sold_email:\s(?<old_email_address>[a-zA-Z0-9_.+=:-]+@[0-9A-Za-z][0-9A-Za-z-]{0,62}(?:\.(?:[0-9A-Za-z][0-9A-Za-z-]{0,62})));\sroles:\s(?<user_role>\w.+);\sold_roles:\s(?<old_role>\w.+)" } 
		add_tag => "account_edited"
	}
	## User account deletion
	grok {
		match => { "message" => "(?<event_time>%{YEAR}-%{MONTHNUM}-%{MONTHDAY}\s%{TIME})\s\w+\s%{HOSTNAME:host_name}\s.*\s:\s(?<log_level>\w+):\s(?<deleted_by>\w+),\s%{IPORHOST:src_ip};\s(?<msg>User account deleted);\sID:\s(?<deleted_user_id>\d+)" }
		add_tag => "account_deleted"
	}
	## File Uploads
	grok {
		match => { "message" => "(?<event_time>%{YEAR}-%{MONTHNUM}-%{MONTHDAY}\s%{TIME})\s\w+\s%{HOSTNAME:host_name}\s.*\s:\s(?<log_level>\w+):\s(?<user_name>\w+),\s%{IPORHOST:src_ip};\s(?<msg>Media file added);\sID:\s(?<file_id>\d+);\sname:\s(?<file_name>\w+);\stype:\s(?<file_type>\w.+)" }
		add_tag => "file_added"
	}
	## File Deletion
	grok {
		match => { "message" => "(?<event_time>%{YEAR}-%{MONTHNUM}-%{MONTHDAY}\s%{TIME})\s\w+\s%{HOSTNAME:host_name}\s.*\s:\s(?<log_level>\w+):\s(?<user_name>\w+),\s%{IPORHOST:src_ip};\s(?<msg>Post deleted).*Post\sid:\s(?<file_id>\d+)" }
		add_tag => "file_deleted"
	}
	## Theme Activations
	grok {
		match => { "message" => "(?<event_time>%{YEAR}-%{MONTHNUM}-%{MONTHDAY}\s%{TIME})\s\w+\s%{HOSTNAME:host_name}\s.*\s:\s(?<log_level>\w+):\s(?<user_name>\w+),\s%{IPORHOST:src_ip};\s(?<msg>Theme.*):\s(?<theme_name>\w.+)" }
		add_tag => "theme_changes"
	}
	## Widget management
	grok {
		match => { "message" => "(?<event_time>%{YEAR}-%{MONTHNUM}-%{MONTHDAY}\s%{TIME})\s\w+\s%{HOSTNAME:host_name}\s.*\s:\s(?<log_level>\w+):\s(?<user_name>\w+),\s%{IPORHOST:src_ip};\s(?<msg>Widget.*)" }
		add_tag => "widget_changes"
	}
}
output {
   elasticsearch {
     hosts => ["elasticsearch:9200"]
     index => "wordpress-%{+YYYY.MM.dd}"
   }
  #stdout { codec => rubydebug }
}
