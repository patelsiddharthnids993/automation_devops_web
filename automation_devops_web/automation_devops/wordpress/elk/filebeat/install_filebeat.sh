apt-get update
apt-get upgrade -y
apt-get -y install gnupg
apt-get -y install curl tcpdump telnet iputils-ping sudo wget vim net-tools
wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch | sudo apt-key add -
apt-get -y install apt-transport-https
echo "deb https://artifacts.elastic.co/packages/7.x/apt stable main" | sudo tee  /etc/apt/sources.list.d/elastic-7.x.list
apt-get update
apt-get -y install filebeat
cp /etc/filebeat/filebeat.yml /etc/filebeat/filebeat.yml_orig
cp filebeat.yml /etc/filebeat/filebeat.yml
#filebeat setup --index-management -E output.logstash.enabled=false -E 'output.elasticsearch.hosts=["elasticsearch:9200"]'
#filebeat setup -e -E output.logstash.enabled=false -E output.elasticsearch.hosts=['elasticsearch:9200'] -E setup.kibana.host=kibana:5601
service filebeat start
service filebeat status
mkdir /var/log/wordpress/
chown -R www-data: /var/log/wordpress
