echo "Starting the server"
sudo docker-compose up -d
sleep 2
echo '------------------------------------------------------'

echo "copying dev.sh to Dev Server"
sudo docker cp dev.sh dev:/home
sleep 2
echo '------------------------------------------------------'

echo "copying agent.sh to jenkins agent server"
sudo docker cp agent.sh jenkinsagent:/home
sleep 2
echo '------------------------------------------------------'

echo "executing dev.sh script"
sudo docker exec dev bash /home/dev.sh
sleep 2
echo '------------------------------------------------------'

echo "executing agent.sh script"
sudo docker exec jenkinsagent bash /home/agent.sh
sleep 2
echo '------------------------------------------------------'
