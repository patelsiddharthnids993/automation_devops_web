echo "Installing docker" 
sudo apt update
sudo apt -y install docker.io
sudo systemctl enable --now docker
sudo systemctl is-active docker
sudo chown ubuntu:ubuntu /var/run/docker.sock
echo "============================================================================================================"

echo "Installing kubectl"
curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"
chmod +x kubectl
sudo mv ./kubectl /usr/local/bin/kubectl
kubectl version --client
echo "============================================================================================================"

echo "Installing kind"
curl -Lo ./kind https://kind.sigs.k8s.io/dl/v0.11.1/kind-linux-amd64
chmod +x ./kind
sudo mv ./kind /usr/local/bin
which kind
kind version
echo "============================================================================================================"

echo "Creating k8s cluster"
kind create cluster --name k8s-kind-cluster1
echo "============================================================================================================"

echo "Getting the cluster details"
kubectl cluster-info --context kind-k8s-kind-cluster1
echo "============================================================================================================"
